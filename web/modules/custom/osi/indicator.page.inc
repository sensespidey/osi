<?php

/**
 * @file
 * Contains indicator.page.inc.
 *
 * Page callback for Indicator entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Indicator templates.
 *
 * Default template: indicator.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_indicator(array &$variables) {
  // Fetch Indicator Entity Object.
  $indicator = $variables['elements']['#indicator'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}

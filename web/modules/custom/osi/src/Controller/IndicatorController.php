<?php

namespace Drupal\osi\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Url;
use Drupal\osi\Entity\IndicatorInterface;

/**
 * Class IndicatorController.
 *
 *  Returns responses for Indicator routes.
 */
class IndicatorController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * Displays a Indicator  revision.
   *
   * @param int $indicator_revision
   *   The Indicator  revision ID.
   *
   * @return array
   *   An array suitable for drupal_render().
   */
  public function revisionShow($indicator_revision) {
    $indicator = $this->entityManager()->getStorage('indicator')->loadRevision($indicator_revision);
    $view_builder = $this->entityManager()->getViewBuilder('indicator');

    return $view_builder->view($indicator);
  }

  /**
   * Page title callback for a Indicator  revision.
   *
   * @param int $indicator_revision
   *   The Indicator  revision ID.
   *
   * @return string
   *   The page title.
   */
  public function revisionPageTitle($indicator_revision) {
    $indicator = $this->entityManager()->getStorage('indicator')->loadRevision($indicator_revision);
    return $this->t('Revision of %title from %date', ['%title' => $indicator->label(), '%date' => format_date($indicator->getRevisionCreationTime())]);
  }

  /**
   * Generates an overview table of older revisions of a Indicator .
   *
   * @param \Drupal\osi\Entity\IndicatorInterface $indicator
   *   A Indicator  object.
   *
   * @return array
   *   An array as expected by drupal_render().
   */
  public function revisionOverview(IndicatorInterface $indicator) {
    $account = $this->currentUser();
    $langcode = $indicator->language()->getId();
    $langname = $indicator->language()->getName();
    $languages = $indicator->getTranslationLanguages();
    $has_translations = (count($languages) > 1);
    $indicator_storage = $this->entityManager()->getStorage('indicator');

    $build['#title'] = $has_translations ? $this->t('@langname revisions for %title', ['@langname' => $langname, '%title' => $indicator->label()]) : $this->t('Revisions for %title', ['%title' => $indicator->label()]);
    $header = [$this->t('Revision'), $this->t('Operations')];

    $revert_permission = (($account->hasPermission("revert all indicator revisions") || $account->hasPermission('administer indicator entities')));
    $delete_permission = (($account->hasPermission("delete all indicator revisions") || $account->hasPermission('administer indicator entities')));

    $rows = [];

    $vids = $indicator_storage->revisionIds($indicator);

    $latest_revision = TRUE;

    foreach (array_reverse($vids) as $vid) {
      /** @var \Drupal\osi\IndicatorInterface $revision */
      $revision = $indicator_storage->loadRevision($vid);
      // Only show revisions that are affected by the language that is being
      // displayed.
      if ($revision->hasTranslation($langcode) && $revision->getTranslation($langcode)->isRevisionTranslationAffected()) {
        $username = [
          '#theme' => 'username',
          '#account' => $revision->getRevisionUser(),
        ];

        // Use revision link to link to revisions that are not active.
        $date = \Drupal::service('date.formatter')->format($revision->getRevisionCreationTime(), 'short');
        if ($vid != $indicator->getRevisionId()) {
          $link = $this->l($date, new Url('entity.indicator.revision', ['indicator' => $indicator->id(), 'indicator_revision' => $vid]));
        }
        else {
          $link = $indicator->link($date);
        }

        $row = [];
        $column = [
          'data' => [
            '#type' => 'inline_template',
            '#template' => '{% trans %}{{ date }} by {{ username }}{% endtrans %}{% if message %}<p class="revision-log">{{ message }}</p>{% endif %}',
            '#context' => [
              'date' => $link,
              'username' => \Drupal::service('renderer')->renderPlain($username),
              'message' => ['#markup' => $revision->getRevisionLogMessage(), '#allowed_tags' => Xss::getHtmlTagList()],
            ],
          ],
        ];
        $row[] = $column;

        if ($latest_revision) {
          $row[] = [
            'data' => [
              '#prefix' => '<em>',
              '#markup' => $this->t('Current revision'),
              '#suffix' => '</em>',
            ],
          ];
          foreach ($row as &$current) {
            $current['class'] = ['revision-current'];
          }
          $latest_revision = FALSE;
        }
        else {
          $links = [];
          if ($revert_permission) {
            $links['revert'] = [
              'title' => $this->t('Revert'),
              'url' => $has_translations ?
              Url::fromRoute('entity.indicator.translation_revert', ['indicator' => $indicator->id(), 'indicator_revision' => $vid, 'langcode' => $langcode]) :
              Url::fromRoute('entity.indicator.revision_revert', ['indicator' => $indicator->id(), 'indicator_revision' => $vid]),
            ];
          }

          if ($delete_permission) {
            $links['delete'] = [
              'title' => $this->t('Delete'),
              'url' => Url::fromRoute('entity.indicator.revision_delete', ['indicator' => $indicator->id(), 'indicator_revision' => $vid]),
            ];
          }

          $row[] = [
            'data' => [
              '#type' => 'operations',
              '#links' => $links,
            ],
          ];
        }

        $rows[] = $row;
      }
    }

    $build['indicator_revisions_table'] = [
      '#theme' => 'table',
      '#rows' => $rows,
      '#header' => $header,
    ];

    return $build;
  }

}

<?php

namespace Drupal\osi\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Indicator entities.
 *
 * @ingroup osi
 */
interface IndicatorInterface extends ContentEntityInterface, RevisionLogInterface, EntityChangedInterface, EntityOwnerInterface {

  // Add get/set methods for your configuration properties here.

  /**
   * Gets the Indicator name.
   *
   * @return string
   *   Name of the Indicator.
   */
  public function getName();

  /**
   * Sets the Indicator name.
   *
   * @param string $name
   *   The Indicator name.
   *
   * @return \Drupal\osi\Entity\IndicatorInterface
   *   The called Indicator entity.
   */
  public function setName($name);

  /**
   * Gets the Indicator creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Indicator.
   */
  public function getCreatedTime();

  /**
   * Sets the Indicator creation timestamp.
   *
   * @param int $timestamp
   *   The Indicator creation timestamp.
   *
   * @return \Drupal\osi\Entity\IndicatorInterface
   *   The called Indicator entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Indicator published status indicator.
   *
   * Unpublished Indicator are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Indicator is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Indicator.
   *
   * @param bool $published
   *   TRUE to set this Indicator to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\osi\Entity\IndicatorInterface
   *   The called Indicator entity.
   */
  public function setPublished($published);

  /**
   * Gets the Indicator revision creation timestamp.
   *
   * @return int
   *   The UNIX timestamp of when this revision was created.
   */
  public function getRevisionCreationTime();

  /**
   * Sets the Indicator revision creation timestamp.
   *
   * @param int $timestamp
   *   The UNIX timestamp of when this revision was created.
   *
   * @return \Drupal\osi\Entity\IndicatorInterface
   *   The called Indicator entity.
   */
  public function setRevisionCreationTime($timestamp);

  /**
   * Gets the Indicator revision author.
   *
   * @return \Drupal\user\UserInterface
   *   The user entity for the revision author.
   */
  public function getRevisionUser();

  /**
   * Sets the Indicator revision author.
   *
   * @param int $uid
   *   The user ID of the revision author.
   *
   * @return \Drupal\osi\Entity\IndicatorInterface
   *   The called Indicator entity.
   */
  public function setRevisionUserId($uid);

}

<?php

namespace Drupal\osi\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Render controller for Indicator questions.
 */
class IndicatorViewBuilder extends EntityViewBuilder {

  /**
   * The entity repository.
   *
   * @var \Drupal\Core\Entity\EntityRepository
   */
  protected $entityRepository;

  /**
   * {@inheritdoc}
   */
  public function __construct($entity_type, $entity_manager, $language_manager, EntityRepository $entity_repository, $theme_registry = NULL) {
    parent::__construct($entity_type, $entity_manager, $language_manager, $theme_registry);
    $this->entityRepository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function createInstance(ContainerInterface $container, EntityTypeInterface $entity_type) {
    return new static(
      $entity_type,
      $container->get('entity.manager'),
      $container->get('language_manager'),
      $container->get('entity.repository'),
      $container->get('theme.registry')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function view(EntityInterface $entity, $view_mode = 'full', $langcode = NULL) {
    $entity = $this->entityRepository->getTranslationFromContext($entity, $langcode);

    $output = parent::view($entity, $view_mode, $langcode);
    $output['#attributes']['class'][] = 'indicator-view';
    $output['#attributes']['class'][] = $view_mode;

    $output['#indicator'] = $entity;
    $output['indicator'] = array(
      '#lazy_builder' => [
        'indicator.post_render_cache:renderViewForm',
        [
          'id' => $entity->id(),
          'view_mode' => $view_mode,
          'langcode' => $entity->language()->getId(),
        ],
      ],
      '#create_placeholder' => TRUE,
      '#cache' => [
        'tags' => $entity->getCacheTags(),
      ],
    );

    dsm("up in your views");

    return $output;
  }
}

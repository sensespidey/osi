<?php

namespace Drupal\osi\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\user\UserInterface;

/**
 * Defines the Indicator entity.
 *
 * @ingroup osi
 *
 * @ContentEntityType(
 *   id = "indicator",
 *   label = @Translation("Indicator"),
 *   handlers = {
 *     "storage" = "Drupal\osi\IndicatorStorage",
 *     "view_builder" = "Drupal\osi\Entity\IndicatorViewBuilder",
 *     "list_builder" = "Drupal\osi\IndicatorListBuilder",
 *     "views_data" = "Drupal\osi\Entity\IndicatorViewsData",
 *     "translation" = "Drupal\osi\IndicatorTranslationHandler",
 *
 *     "form" = {
 *       "default" = "Drupal\osi\Form\IndicatorForm",
 *       "add" = "Drupal\osi\Form\IndicatorForm",
 *       "edit" = "Drupal\osi\Form\IndicatorForm",
 *       "delete" = "Drupal\osi\Form\IndicatorDeleteForm",
 *     },
 *     "access" = "Drupal\osi\IndicatorAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\osi\IndicatorHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "indicator",
 *   data_table = "indicator_field_data",
 *   revision_table = "indicator_revision",
 *   revision_data_table = "indicator_field_revision",
 *   translatable = TRUE,
 *   admin_permission = "administer indicator entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "vid",
 *     "label" = "name",
 *     "uuid" = "uuid",
 *     "uid" = "user_id",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/indicator/{indicator}",
 *     "add-form" = "/admin/structure/indicator/add",
 *     "edit-form" = "/admin/structure/indicator/{indicator}/edit",
 *     "delete-form" = "/admin/structure/indicator/{indicator}/delete",
 *     "version-history" = "/admin/structure/indicator/{indicator}/revisions",
 *     "revision" = "/admin/structure/indicator/{indicator}/revisions/{indicator_revision}/view",
 *     "revision_revert" = "/admin/structure/indicator/{indicator}/revisions/{indicator_revision}/revert",
 *     "revision_delete" = "/admin/structure/indicator/{indicator}/revisions/{indicator_revision}/delete",
 *     "translation_revert" = "/admin/structure/indicator/{indicator}/revisions/{indicator_revision}/revert/{langcode}",
 *     "collection" = "/admin/structure/indicator",
 *   },
 *   field_ui_base_route = "indicator.settings"
 * )
 */
class Indicator extends RevisionableContentEntityBase implements IndicatorInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function urlRouteParameters($rel) {
    $uri_route_parameters = parent::urlRouteParameters($rel);

    if ($rel === 'revision_revert' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }
    elseif ($rel === 'revision_delete' && $this instanceof RevisionableInterface) {
      $uri_route_parameters[$this->getEntityTypeId() . '_revision'] = $this->getRevisionId();
    }

    return $uri_route_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    foreach (array_keys($this->getTranslationLanguages()) as $langcode) {
      $translation = $this->getTranslation($langcode);

      // If no owner has been set explicitly, make the anonymous user the owner.
      if (!$translation->getOwner()) {
        $translation->setOwnerId(0);
      }
    }

    // If no revision author has been set explicitly, make the indicator owner the
    // revision author.
    if (!$this->getRevisionUser()) {
      $this->setRevisionUserId($this->getOwnerId());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return $this->get('name')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setName($name) {
    $this->set('name', $name);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? TRUE : FALSE);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Authored by'))
      ->setDescription(t('The user ID of author of the Indicator entity.'))
      ->setRevisionable(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setTranslatable(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 0,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 5,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'autocomplete_type' => 'tags',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Question'))
      ->setDescription(t('The indicator question.'))
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDefaultValue('')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => -4,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['allow_na'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Allow N/A'))
      ->setDescription(t('Whether to provide a N/A option'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(FALSE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
      ]);

    $config = \Drupal::configFactory()->get('indicator.settings');
    $answers = $config->get('baseFieldDefinitions answers');
    dsm($answers, "getAnswers");

#    $fields['standard'] = BaseFieldDefinition::create('integer')
#      ->setLabel(t('Meets Standard'))
#      ->setDescription(t('Score required to meet the standard.'))
#      ->setRevisionable(TRUE)
#      ->setRequired(TRUE);
#      #->setDefaultValue()
#
#    $fields['exceeds'] = BaseFieldDefinition::create('integer')
#      ->setLabel(t('Exceeds Standard'))
#      ->setDescription(t('Score required to exceed the standard.'))
#      ->setRevisionable(TRUE)
#      ->setRequired(TRUE);
    #->setDefaultValue()


    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Indicator is published.'))
      ->setRevisionable(TRUE)
      ->setDefaultValue(TRUE)
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'weight' => -3,
      ]);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    $fields['revision_translation_affected'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Revision translation affected'))
      ->setDescription(t('Indicates if the last edit of a translation belongs to current revision.'))
      ->setReadOnly(TRUE)
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE);

    return $fields;
  }

  public function getOptions() {
    $answers = \Drupal::config('indicator.settings')->get('answers');
    $options = array();

    foreach (explode("\n", $answers) as $line) {
      $line = trim($line);
      list($key,$value) = explode('|', $line, 2);

      $options[$key] = $value;
    }

    #dsm($this, "indicator object");

    if ($this->get('allow_na')->first()->getValue()['value']) {
      $options['-1'] = t('N/A');
    }

    #if ($this->allow_na)

    return $options;
  }
}

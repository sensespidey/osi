<?php

namespace Drupal\osi\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\BaseFormIdInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\osi\Entity\IndicatorInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Renders question choice form for an Indicator.
 */
class IndicatorViewForm extends FormBase implements BaseFormIdInterface {

  /**
   * The Indicator to render.
   *
   * @var \Drupal\osi\Entity\IndicatorInterface
   */
  protected $indicator;

  /**
   * {@inheritdoc}
   */
  public function getBaseFormId() {
    return 'indicator_view_form';
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'indicator_view_form_' . $this->indicator->id();
  }

  /**
   * Set the Indicator for this form.
   *
   * @param \Drupal\osi\Entity\EntityInterface $indicator
   *   The indicator that will be set in the form.
   */
  public function setIndicator(IndicatorInterface $indicator) {
    $this->indicator = $indicator;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL, $view_mode = 'full') {
    // Add the indicator to the form.
    $form['iid'] = [
      '#type' => value,
      '#value' => $this->indicator->id(),
    ];

    $form['#view_mode'] = $view_mode;

    $options = $this->indicator->getOptions();

    $form['choice'] = [
      '#type' => 'radios',
      '#title' => t('Choices'),
      '#title_display' => 'invisible',
      '#ajax' => array( 'callback' => '::ajaxSaveAnswer' ),
      '#options' => $options,
    ];

    // Look up or create a "take" for this user (rid)
    $rid = \Drupal::service('indicator_answer.storage')->getRid();
    dsm($rid, "rid");

    $form['rid'] = [
      '#type' => 'value',
      '#value' => $rid,
    ];

    $answer = \Drupal::service('indicator_answer.storage')->getAnswer($this->indicator->id(), $rid);

    if (!empty($answer->aid)) {
      $form['aid'] = [
        '#type' => 'value',
        '#value' => $answer->aid,
      ];
      $form['choice']['#default_value'] = $answer->answer;
    }

    $form['#theme'] = 'indicator_view';
    $form['#entity'] = $this->indicator;
    $form['#action'] = $this->indicator->url(
      'canonical',
      ['query' => \Drupal::destination()->getAsArray()]
    );

    #$form['actions'] = $this->actions($form, $form_state, $this->indicator);

    $form['#cache'] = [
      'tags' => $this->indicator->getCacheTags(),
    ];

    return $form;
  }

  public function ajaxSaveAnswer(array $form, FormStateInterface $form_state) {
    dsm($form, "Form");
    dsm($form_state, "form_state");

    $rid = $form_state->getValue('rid');
    $iid = $form_state->getValue('iid');
    $aid = $form_state->getValue('aid');
    $answer = $form_state->getValue('choice');

    if (empty($aid)) {
      \Drupal::service('indicator_answer.storage')->saveAnswer($answer, $iid, $rid);
    } else {
      \Drupal::service('indicator_answer.storage')->updateAnswer($answer, $aid);
    }

    $response = new AjaxResponse();
    $response->addCommand(new InvokeCommand('.indicator-view-form', 'addClass', array('answered')));
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }
}

<?php

namespace Drupal\osi\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class IndicatorSettingsForm.
 *
 * @ingroup osi
 */
class IndicatorSettingsForm extends ConfigFormBase {

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'indicator_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'indicator.settings',
    ];
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('indicator.settings')
      ->set('answers', $values['answers'])
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Defines the settings form for Indicator entities.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form definition array.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('indicator.settings');

    $form['indicator_settings']['#markup'] = 'Settings form for Indicator entities. Manage field settings here.';

    $form['answers'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('List of possible answers'),
      '#description' => $this->t('The possible values this field can contain. Enter one value per line, in the format key|label.\n
The key is the stored value. The label will be used in displayed values and edit forms.\n
The label is optional: if a line contains a single string, it will be used as key and label.'),
      '#default_value' => $config->get('answers'),
      '#required' => TRUE,
    );

    return parent::buildForm($form, $form_state);
  }

}

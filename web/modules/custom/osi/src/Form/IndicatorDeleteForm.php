<?php

namespace Drupal\osi\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Indicator entities.
 *
 * @ingroup osi
 */
class IndicatorDeleteForm extends ContentEntityDeleteForm {


}

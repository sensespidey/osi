<?php

namespace Drupal\osi;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Indicator entity.
 *
 * @see \Drupal\osi\Entity\Indicator.
 */
class IndicatorAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\osi\Entity\IndicatorInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished indicator entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published indicator entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit indicator entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete indicator entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add indicator entities');
  }

}

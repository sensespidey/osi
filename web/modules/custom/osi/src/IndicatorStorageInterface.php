<?php

namespace Drupal\osi;

use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\osi\Entity\IndicatorInterface;

/**
 * Defines the storage handler class for Indicator entities.
 *
 * This extends the base storage class, adding required special handling for
 * Indicator entities.
 *
 * @ingroup osi
 */
interface IndicatorStorageInterface extends ContentEntityStorageInterface {

  /**
   * Gets a list of Indicator revision IDs for a specific Indicator.
   *
   * @param \Drupal\osi\Entity\IndicatorInterface $entity
   *   The Indicator entity.
   *
   * @return int[]
   *   Indicator revision IDs (in ascending order).
   */
  public function revisionIds(IndicatorInterface $entity);

  /**
   * Gets a list of revision IDs having a given user as Indicator author.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user entity.
   *
   * @return int[]
   *   Indicator revision IDs (in ascending order).
   */
  public function userRevisionIds(AccountInterface $account);

  /**
   * Counts the number of revisions in the default language.
   *
   * @param \Drupal\osi\Entity\IndicatorInterface $entity
   *   The Indicator entity.
   *
   * @return int
   *   The number of revisions in the default language.
   */
  public function countDefaultLanguageRevisions(IndicatorInterface $entity);

  /**
   * Unsets the language for all Indicator with the given language.
   *
   * @param \Drupal\Core\Language\LanguageInterface $language
   *   The language object.
   */
  public function clearRevisionsLanguage(LanguageInterface $language);

}

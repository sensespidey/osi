<?php

namespace Drupal\osi;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for indicator.
 */
class IndicatorTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}

<?php

namespace Drupal\osi;

use Drupal\Core\Entity\Sql\SqlContentEntityStorage;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\osi\Entity\IndicatorInterface;

/**
 * Defines the storage handler class for Indicator entities.
 *
 * This extends the base storage class, adding required special handling for
 * Indicator entities.
 *
 * @ingroup osi
 */
class IndicatorStorage extends SqlContentEntityStorage implements IndicatorStorageInterface {

  /**
   * {@inheritdoc}
   */
  public function revisionIds(IndicatorInterface $entity) {
    return $this->database->query(
      'SELECT vid FROM {indicator_revision} WHERE id=:id ORDER BY vid',
      [':id' => $entity->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function userRevisionIds(AccountInterface $account) {
    return $this->database->query(
      'SELECT vid FROM {indicator_field_revision} WHERE uid = :uid ORDER BY vid',
      [':uid' => $account->id()]
    )->fetchCol();
  }

  /**
   * {@inheritdoc}
   */
  public function countDefaultLanguageRevisions(IndicatorInterface $entity) {
    return $this->database->query('SELECT COUNT(*) FROM {indicator_field_revision} WHERE id = :id AND default_langcode = 1', [':id' => $entity->id()])
      ->fetchField();
  }

  /**
   * {@inheritdoc}
   */
  public function clearRevisionsLanguage(LanguageInterface $language) {
    return $this->database->update('indicator_revision')
      ->fields(['langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED])
      ->condition('langcode', $language->getId())
      ->execute();
  }

}

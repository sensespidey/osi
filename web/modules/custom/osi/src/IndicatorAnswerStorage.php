<?php

namespace Drupal\osi;

use Drupal\Core\Cache\CacheTagsInvalidatorInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountInterface;

/**
 * Service class for indicator answer storage.
 */
class IndicatorAnswerStorage {
  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The cache tags invalidator.
   *
   * @var \Drupal\Core\Cache\CacheTagsInvalidatorInterface
   */
  protected $cacheTagsInvalidator;

  /**
   * Constructs a new PollVoteStorage.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   A Database connection to use for reading and writing database data.
   * @param \Drupal\Core\Cache\CacheTagsInvalidatorInterface $cache_tags_invalidator
   *   The cache tags invalidator.
   */
  public function __construct(Connection $connection, CacheTagsInvalidatorInterface $cache_tags_invalidator) {
    $this->connection = $connection;
    $this->cacheTagsInvalidator = $cache_tags_invalidator;
  }

  public function getRid() {
    $user = \Drupal::currentUser();

    $result = $this->connection->query('SELECT rid FROM {osi_take} WHERE uid=:uid ORDER BY rid DESC',
      array(':uid' => $user->id())
    )->fetch();

    if (!empty($result->rid)) {
      $rid = $result->rid;
    } else {
      $rid = $this->connection->insert('osi_take')
        ->fields(array(
          'uid' => $user->id(),
          'created' => time(),
        ))->execute();
    }

    return $rid;
  }

  public function getAnswer($iid, $rid = FALSE) {
    if (empty($rid)) {
      $rid = $this->getRid();
    }

    $result = $this->connection->query('SELECT aid,answer FROM {osi_answer} WHERE rid=:rid AND iid=:iid',
      array(':rid' => $rid, ':iid' => $iid)
    )->fetch();

    return $result;
  }

  public function saveAnswer($answer, $iid, $rid) {
    $this->connection->merge('osi_answer')
      ->keys([
        'iid' => $iid,
        'rid' => $rid,
      ])
      ->insertFields([
        'answer' => $answer,
        'iid' => $iid,
        'rid' => $rid,
        'created' => time(),
      ])
      ->updateFields([
        'answer' => $answer,
        'changed' => time(),
      ])->execute();
  }

  public function updateAnswer($answer, $aid) {
    $this->connection->update('osi_answer')
      ->fields([
        'answer' => $answer,
        'changed' => time(),
      ])
      ->condition('aid', $aid)
      ->execute();
  }
}

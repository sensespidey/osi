<?php

namespace Drupal\osi;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\osi\Entity\IndicatorInterface;

/**
 * Defines a service for indicator post render cache callbacks.
 */
class IndicatorPostRenderCache {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a new IndicatorPostRenderCache object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Callback for #post_render_cache; replaces placeholder with indicator view form.
   *
   * @param int $id
   *   The indicator ID.
   * @param string $view_mode
   *   The view mode the indicator should be rendered with.
   * @param string $langcode
   *   The langcode in which the indicator should be rendered.
   *
   * @return array
   *   A renderable array containing the indicator form.
   */
  public function renderViewForm($id, $view_mode, $langcode = NULL) {
    /** @var \Drupal\osi\Entity\IndicatorInterface $indicator*/
    $indicator = $this->entityTypeManager->getStorage('indicator')->load($id);

    if ($indicator) {
      if ($langcode && $indicator->hasTranslation($langcode)) {
        $indicator = $indicator->getTranslation($langcode);
      }
      /** @var \Drupal\osi\Form\IndicatorViewForm $form_object */
      $form_object = \Drupal::service('class_resolver')->getInstanceFromDefinition('Drupal\osi\Form\IndicatorViewForm');
      $form_object->setIndicator($indicator);
      return \Drupal::formBuilder()->getForm($form_object, \Drupal::request(), $view_mode);
    }
    else {
      return ['#markup' => ''];
    }
  }
}